import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class SimpleDialog extends Component {
    constructor(props) {
        super(props)
        this.handleClose = this.handleClose.bind(this)
        this.show = true
    }

    handleClose = () => {
        this.show = false
        this.setState({}) //fires the render()
    };

    render() {
        if (this.show) { //if we are allowed to be shown...
            return (
                <Dialog
                open={this.props.open} //...button defines if we have to be shown or not
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description">
                    <DialogTitle id="alert-dialog-title">{this.props.Title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {this.props.Text}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        {
                            (this.props.showCancel === "true") ? 
                            <Button onClick={this.handleClose} color="primary">
                            Cancel
                            </Button> : null
                        }
                    
                    <Button onClick={this.handleClose} color="primary" autoFocus>
                        OK
                    </Button>
                    </DialogActions>
              </Dialog>
            )
        
        } else { //we do not show regardless of what button state is
            this.show = true //reset
            return null
        }        
    }
}

class DialogButton extends Component {
    constructor (props) {
        super(props)
        this.state = {open: false}
    }
  
    handleClickOpen = () => {
      this.setState({ open: true });
    };
  
    render() {
      return (
        <div>
          <Button variant="contained" color="primary" onClick={this.handleClickOpen}>
            {this.props.btnText}
          </Button>
          <SimpleDialog 
          open={this.state.open} 
          showCancel={this.props.showCancel} 
          Title={this.props.dlgTitle} 
          Text={this.props.dlgText} />
        </div>
      );
    }
  }
  
  export default DialogButton;